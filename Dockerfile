FROM golang:stretch

WORKDIR $GOPATH/src/gitlab.com/dpr-aquix/ion-sfu

COPY go.mod go.sum ./
RUN cd $GOPATH/src/gitlab.com/dpr-aquix/ion-sfu && go mod download

COPY sfu/ $GOPATH/src/gitlab.com/dpr-aquix/ion-sfu/pkg
COPY cmd/ $GOPATH/src/gitlab.com/dpr-aquix/ion-sfu/cmd
COPY config.toml $GOPATH/src/gitlab.com/dpr-aquix/ion-sfu/config.toml
